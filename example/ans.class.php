<?php

// ToDo: - Public and private to stop private values being sent out.

	class AlmostNoSQL extends mysqli {
		
		/* Set our constants */
		CONST id_field = 'hash';
		CONST key_field = 'key';
		CONST value_field = 'value';
		CONST type_field = 'data_type';
		CONST object_field = 'object';
		CONST table_name = 'data';

        /* Data Type Constants */
        CONST ARR_TYPE = 'array';
        CONST ASSOC_TYPE = 'assoc_array';
        CONST SRT_TYPE = 'string';

		/* Magic functions */
		public function __contruct($host = NULL, $user = NULL, $password = NULL, $database = NULL){
			parent::__construct($host, $user, $pass, $db);
		}
		
		public function __set($name,$value){
			$this->$name = $value;
		}
		
		public function __get($name){
			return $this->$name;
		}
		/* End Magic */
		
		/* Get a single value from an object */
		public function getValue($hash,$key){
			$q = $this->query("SELECT `".self::value_field."` FROM `".self::table_name."` WHERE `".self::key_field."` = '".$key."' AND `".self::id_field."` = '".$hash."' LIMIT 1");
			if($q){
				$res = $q->fetch_assoc();
				return $res[self::value_field];
			} else { return false; }
		}
		
		/* get a whole object */
		public function getObject($hash,$object){
			$q = $this->query("SELECT `".self::key_field."`,`".self::value_field."`,`".self::type_field."` FROM `".self::table_name."` WHERE `".self::object_field."` = '".$object."' AND `".self::id_field."` = '".$hash."'");
            $user = array();
            if($q){
				while($row = $q->fetch_assoc()){
                    if($row[self::type_field] == self::ARR_TYPE){
                        if(isset($user[$row[self::key_field]])){
                            $user[$row[self::key_field]][] = $row[self::value_field];
                        } else {
                            $user[$row[self::key_field]] = array($row[self::value_field]);
                        }
                    } else if($row[self::type_field] == self::ASSOC_TYPE){
                        $user[$row[self::key_field]] = $this->getObject($row[self::value_field],$row[self::key_field]);
                    } else {
                        $user[$row[self::key_field]] = $row[self::value_field];
                    }
                }
				return (object)$user;
			} else { return false; }
		}
	}
	
?>
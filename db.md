# Almost No SQL
ANS is an approach to store data in SQL tables without enforcing a schema on that data. Each table row relates to a key/value pair on an object which is identified by a unique hash value. This value is unique across ALL objects as such the query does not need to know the object type to select the objects data, however for speed reasons it is advisable to pass it if you know it.

This implementation of ANS used the following SQL Schema for its main table.

 ________________________________
|   hash    |  varchar(255), key |
|  object   |  varchar(255), key |
| data_type |  varchar(255)      |
|    key    |  varchar(255), key |
|   value   |        text        |
|   public  |      int (1)       |
 --------------------------------

## Hash
Hash is used as the global identifier for objects, it is left to you to ensure this value is unique SQL should not be forced to check for unique values on this field as a hash will be used on many rows (one for each key/value pair in your object) the size of this hash is up to you but the higher entropy of this value the more chance you have of avoiding conflicts.

## Object
Object is the type of object you are storing, this of this as 'table name' if coming from a SQL background or a `collection` if coming from NoSQL.

## Data Type
Data type is used to outline the type of data that key is on your object, you can use this in your code to implement arrays and assoc arrays of keys/values. See the included PHP class for an examples of this.

## Key
The key for this value on the object.

## Value
This is your value, this field is stored as text for simplicity here as most data you're likely to store will be text values or serialized data. However, you could set this field to blob if you wanted to store other data.

## Public
This is used as a public private flag, you can use this in your code to stop private data making it to objects.

# Complexity
This is a note about complexity of queries, with this method you really can't get too clever with your lookups. No joins or map reduction, if you need complex cross table queries use SQL if you need large data set reduction use a NoSQL solution. This is a middle ground for simple key/value storage.

Of course if you maintained your whole table in memory and got clever with your code implementation ir might not suck full balls.

# Performance
ToDo:- Benchmark this at high scale.
The theory here is quite sound, with indexed hash and key values and good use of horizontal partitioning you should be able to achieve high availability of data at volume, with a memory cache in the mix (such as memcahed) and a cache key of your query I'm imagining faster speeds than traditional SQL for many queries. Of course as explained above your queries can't be too complex and if you attempt to search via value on a 1,000,000 row table expect your query to throw a slow_query log entry unless you're running it from hal.

One way around this is to maintain index tables of lookup data with hashes with which you can reach back into your data table and recover the record. You could keep these lookup tables in memory and full text searching wouldn't be too painfull. How exactly you implement these is left up to you.



